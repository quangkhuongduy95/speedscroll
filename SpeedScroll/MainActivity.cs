﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace SpeedScroll
{
    [Activity(Label = "SpeedScroll", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            //Button button = FindViewById<Button>(Resource.Id.myButton);

            //button.Click += delegate { button.Text = $"{count++} clicks!"; };

            LinearLayout linearLayout = FindViewById<LinearLayout>(Resource.Id.groudView);

            MyHorizontalScrollView myHorizontalScroll = new MyHorizontalScrollView(this);
            Android.Views.ViewGroup.LayoutParams layoutParams = new Android.Views.ViewGroup.LayoutParams(Android.Views.ViewGroup.LayoutParams.MatchParent
                                                                                                        , Android.Views.ViewGroup.LayoutParams.MatchParent);
            myHorizontalScroll.LayoutParameters = layoutParams;

            linearLayout.AddView(myHorizontalScroll);


            Android.Views.ViewGroup.LayoutParams layoutParams1 = new Android.Views.ViewGroup.LayoutParams(Android.Views.ViewGroup.LayoutParams.MatchParent
                                                                                                        , Android.Views.ViewGroup.LayoutParams.MatchParent);
            LinearLayout linear = new LinearLayout(this);
            linear.LayoutParameters = layoutParams1;

            myHorizontalScroll.AddView(linear);

            for(int i = 1; i< 101;i++)
            {

                Android.Views.ViewGroup.LayoutParams layoutParamsButton = new Android.Views.ViewGroup.LayoutParams(Android.Views.ViewGroup.LayoutParams.WrapContent, 150);

                Button button = new Button(this);
                button.LayoutParameters = layoutParamsButton;
                button.Text = $"btn {i}";
                linear.AddView(button);
            }

        }
    }
}

