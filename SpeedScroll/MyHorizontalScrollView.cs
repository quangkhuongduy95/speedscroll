﻿using System;
using Android.Content;
using Android.Widget;

namespace SpeedScroll
{
    public class MyHorizontalScrollView : HorizontalScrollView
    {
        private static int MAX_SCROLL_SPEED = 500;
        public MyHorizontalScrollView(Context context) : base(context)
        {
        }


        public override void Fling(int velocityX)
        {
            int topVelocityX = (int) (Math.Min(Math.Abs(velocityX), MAX_SCROLL_SPEED) * Math.Sign(velocityX));
            base.Fling(topVelocityX);
            //base.Fling(velocityX);
        }
    }
}
